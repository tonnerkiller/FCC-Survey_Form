FreeCodeCamp.org is a free online Javascript/HTML/CSS course.

This is my attempt of the second project in their curriculum.

Read the project requirements:
https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-survey-form

These source files are deployed here:
https://tonnerkiller.gitlab.io/FCC-Survey_Form
